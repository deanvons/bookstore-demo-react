import logo from './logo.svg';
import './App.css';
import Books from './Components/Book/Books';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import BookDetails from './Components/Book/BookDetails';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Books} />
          <Route path="/details/:id" component={BookDetails} />
        </Switch>

      </BrowserRouter>
    </div>
  );
}

export default App;
