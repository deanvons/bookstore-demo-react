import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getBookById } from '../../utils/api'

function BookDetails() {
    const { id } = useParams()

    const [book, setBook] = useState({})

    useEffect(() => {

        getBookById(id)
            .then(book => {
                setBook(book)
            })
            .catch(error => console.log(error))

    }, [])

    return (<div>
        <h1>{book.title}</h1>
        <img src={book.coverImg} alt="cover" />
        <h3>{book.genre}</h3>

    </div>)
}

export default BookDetails