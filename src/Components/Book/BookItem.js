import { useState } from "react"

function BookItem({ book, bookSelected }) {

    function bookClicked() {
        bookSelected(book.bookId)
    }

    return (
        <div>
            <h4>{book.title}</h4>
            <img src={book.coverImg} alt="cover" onClick={bookClicked} />
        </div>
    )
}

export default BookItem