import { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import { getAllBooks } from "../../utils/api"
import BookItem from "./BookItem"

function Books() {

    const [bookState, setBookState] = useState({
        loaded: true,
        books: [{ bookId: 2, title: "Power of now" }, { bookId: 3, title: "Siddharta" }]
    })

    const history = useHistory()

    useEffect(() => {

        //mounted
        getAllBooks()
            .then(books =>
                //set books to the result
                setBookState({
                    ...bookState,
                    books
                })).catch(error => console.log(error)
                )
    }, [])

    function viewBookDetail(bookId) {
        //route to details page
        history.push(`/details/${bookId}`)
        console.log(bookId)
    }

    const bookListRender = bookState.books.map(b => <BookItem key={b.bookId} book={b} bookSelected={viewBookDetail} />)

    return (
        <div>
            <h1>Book Store</h1>
            <ul>
                {bookListRender}
            </ul>
        </div>
    )
}

export default Books